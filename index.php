<!DOCTYPE html>
<html>
	<head>
		<title>Pousada Ekoo's Beach</title>
		<meta charset="utf-8" />
		
		<link rel="stylesheet" href="css/index.css">
		<link rel="stylesheet" href="css/background.css">
		<link rel="stylesheet" href="css/container.css">
	</head>

	<body>
		<?php include 'language.php' ?>

		<?php include 'menu.php' ?>

		<div id="cnt">
				<img id="img" src="images/fotos/ekoos15-ed.jpg" />
		</div>

		<div class="container">

			<div class="image">
				<img class="img left" src="files/ekoos07.jpg">
				<div class="text">
					<div> A charmosa Pousada <b>Ekoo's Beach</b> está localizada no coração de Tibau do Sul, bairro ao Lado de Pipa onde se encontra o melhor pôr do sol de toda a região. Seguindo sua orla marítima irá encontrar vários pontos turísticos, como Tibau do Sul, Lagoa Guaraíras, Praia do Giz, Praia das Cacimbinhas, Praia do Madeiro, Praia dos Golfinhos, Praia da Pipa e Praia do Amor. São 6,8 km de pura beleza. A nossa propriedade fornece aos hóspedes o conforto familiar que você precisa. </div>
				</div>
			</div>
			<div class="image">
				<a href="acomodacoes.php"><img class="img right" src="files/ekoos28.jpg"></a>
				<div class="text">
					<div> Dispomos de instalações equipadas com TV LCD e ar condicionado, oferecendo um serviço de café da manhã,  internet wi-fi, piscina, churrasqueira na área de lazer e disponível aos hóspedes que procuram preço e qualidade. </div>
					<div> A propriedade possui 9 chalés, cada uma acomoda até 04 pessoas, onde os visitantes são convidados a aproveitar um ambiente descontraído com atendimento diferenciado e familiar. </div>
				</div>
			</div>
			<div class="image">
				<img class="img left" src="images/fotos/index-03.jpg">
				<div class="text"> 
					<div> A região oferece várias opções para diversão, desde passeios de Buggy pelas dunas até passeios de lancha, barco e kaiak pela Lagoa de Guaraíras. Além disso, as opções para desfrutar da culinária regional são várias. Para completar, os barzinhos e boates de Pipa oferecem diversão a noite toda para quem gosta de um agito. </div>
				</div>
			</div>
			
			<br>
			<br>
			
			<?php include 'social.php' ?>

		</div>

		<?php include 'site-design.php' ?>

	</body>
</html>