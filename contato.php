<!DOCTYPE html>
<html>
<head>
		<title>Pousada Ekoo's Beach</title>
		<meta charset="utf-8" />
		
		<link rel="stylesheet" href="css/contato.css">
		<link rel="stylesheet" href="css/background.css">
		<link rel="stylesheet" href="css/container.css">
	</head>

	<body>
		
		<?php
			require_once './swiftmailer/lib/swift_required.php'; // Inclusão da biblioteca que envia e-mails.

			$message = $tel = $cc_myself = ""; // Zerando as variáveis.
			$from = 'redirecionamento.ekoosbeach@gmail.com';

			if ($_SERVER["REQUEST_METHOD"] == "POST") 
			{

				// Pega os valores do formulário.
			   	$message = test_input($_POST["message"]);
			   	$tel = test_input($_POST["tel"]);
			   	$cc_myself = test_input($_POST["cc_myself"]);
			   	$client_mail = test_input($_POST["from"]);

			   	//Enviando do site para o próprio site:
			   	$to = $from;
			   	$message1 = $message . "\n\n --------------- \n Enviado por: " . $client_mail . "\nTelefone: " . $tel;
			   	$message1 = $message1 . "\n Essa email foi gerado pelo formulário de contato do www.ekoosbeach.com de forma automática. \n Por favor, não responda esse email, pois o cliente não irá receber!";

			   	if( enviar_email($message1, $from, $to, $tel) )
			   	{
				   	//Enviando do site para o cliente.
					if($cc_myself == true)
					{
						$message2 = "Esta é uma cópia da mensagem que você enviou para a www.ekoosbeach.com.br. \n" . "--------------- \n\n" . $message;
						$to = $client_mail;
						if( enviar_email($message2, $from, $to, $tel) )
						{
							Redirect('mailsended.php');
						}
						else
						{
							Redirect('error.php');
						}
					}

					Redirect('mailsended.php');
			   	}
			   	else
			   	{
			   		Redirect('error.php');
			   	}
			}

			function test_input($data) {
			   	$data = trim($data);
			   	$data = stripslashes($data);
			   	$data = htmlspecialchars($data);
			   	return $data;
			}

			function enviar_email($message, $from, $to, $tel)
			{
				var_dump($message);
				var_dump($from);
				var_dump($to);
				var_dump($tel);

				$transport = Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, 'ssl')
  				->setUsername('redirecionamento.ekoosbeach@gmail.com')
				->setPassword('Ekoos!123');

				$mailer = Swift_Mailer::newInstance($transport);

				$message = Swift_Message::newInstance('Contato Ekoo\'s Beach')
				->setFrom(array($from))
				->setTo(array($to))
				->setBody($message);

				return $result = $mailer->send($message);
			}

			function Redirect($url, $permanent = false)
			{
		    	header('Location: ' . $url, true, $permanent ? 301 : 302);

		    	exit();
			}
		?>

		<?php include 'language.php' ?>

		<?php include 'menu.php' ?>

		<div class="container segundo">
			<div class="divcontainer" >
				<h2 style="width: 100%; text-align: center"><b> Contato Ekoo's Beach </b></h2>
				<br>
				<br>
				<label class="lab" style="width: 100%; text-align: center"> Para entrar em contato, preencha o formulário abaixo. Em breve responderemos à sua mensagem. </label>
			</div>
				<br>
				<br>
				<br>


			<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
				<br> 
				
				<div class="input-group" style="width: 80%; margin: 0 auto">
					<label class = "small lab"> <label style="color: red"> * </label> Mensagem: </label>
					<br>
					<textarea id="id_message" rows="10" name="message" class="form-control" cols="40"></textarea>
					<br>
				</div>
				<br>
				
				<div class="input-group" style="width: 80%; margin: 0 auto">
					<label class = "small lab"> <label style="color: red"> * </label> Seu e-mail:  </label>
					<br>
					<input id="id_sender" type="email" class="form-control" size="50" name="from"></input>
				</div>
				<br>
				
				<div class="input-group" style="width: 80%; margin: 0 auto">
					<label class = "small lab"> <label style="color: red"> * </label> Telefone:  </label>
					<br>
					<input id="id_tel" type="text" class="form-control" size="15" name="tel"></input>
				</div>
				<br>
				
				<div class="input-group" style="width: 80%; margin: 0 auto">
					<label class = "small lab">
					<label style="color: red; font-size: 11px"> * Campos obrigatórios </label> <br> <br>
					<input id="id_cc_myself" type="checkbox" name="cc_myself" style="margin-right: 10px"> Receber uma copia desta mensagem no meu e-mail.</input> </label>
				</div>
				
				<br>
				
				<div class="input-group" style="width: 80%; margin: 0 auto">
					<input type="submit" value="Enviar" style="margin: 0 auto"></input>
				</div>
			</form>
		</div>
		
		<?php include 'site-design.php' ?>
	</body>
</html>