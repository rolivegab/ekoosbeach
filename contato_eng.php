<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		
		<title>Tibau Paradise</title>
		<!-- Include meta tag to ensure proper rendering and touch zooming -->
		<meta name="viewport" content="width=device-width, initial-scale=1">
		
		
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="css/bootstrap.css">
		<link rel="stylesheet" href="cabecalho.css">
		<link rel="stylesheet" href="contato.css">
		
	</head>

	<body>

		<?php

			require_once "./swiftmailer/lib/swift_required.php"; // Inclusão da biblioteca que envia e-mails.

			$message = $from = $tel = $cc_myself = ""; // Zerando as variáveis.

			if ($_SERVER["REQUEST_METHOD"] == "POST") {

				// Pega os valores do formulário.

			   	$message = test_input($_POST["message"]);
			   	$from = test_input($_POST["from"]);
			   	$tel = test_input($_POST["tel"]);
			   	$cc_myself = test_input($_POST["cc_myself"]);

			   	//Enviando do site para o próprio site.
			   	$message1 = $message . "<br><br> --------------- <br> Enviado por: " . $from . "<br>Telefone: " . $tel;
			   	$to = "oliveira.grdo@gmail.com";

			   	if( enviar_email($message1, "Contato Tibau Paradise", $from, $to, $tel) )
			   	{
				   	//Enviando do site para o cliente.
					if($cc_myself == true)
					{
						$message2 = "This is the copy of the message you sent to the tibauparadise.com.br: <br>" . "--------------- <br><br>" . $message;
						$to = $from;
						$from = "oliveira.grdo@gmail.com";
						if( enviar_email($message2, "Tibau Paradise Contact", $from, $to, $tel) )
						{
							Redirect("mailsended_eng.php");
						}
						else
						{
							Redirect("error_eng.php");
						}
					}

					Redirect("mailsended_eng.php");
			   	}
			   	else
			   	{
			   		Redirect("error_eng.php");
			   	}
			}

			function test_input($data) {
			   	$data = trim($data);
			   	$data = stripslashes($data);
			   	$data = htmlspecialchars($data);
			   	return $data;
			}

			function enviar_email($message, $subject, $from, $to, $tel)
			{
				$transport = Swift_SmtpTransport::newInstance("smtp.gmail.com", 587, "tls")
				->setUsername("oliveira.grdo@gmail.com")
				->setPassword("gro159!123")
				;

			$mailer = Swift_Mailer::newInstance($transport);

			$message = Swift_Message::newInstance()
				->setFrom(array($from))
				->setTo(array($to))
				->setBody($message, "text/html")
				;

				if ($mailer->send($message))
				{
				  	return true;
				}
				else
				{
					return false;
				}
			}

			function Redirect($url, $permanent = false)
			{
		    	header("Location: " . $url, true, $permanent ? 301 : 302);

		    	exit();
			}
		?>

		<div class="language">
			<a class="linkporcima" href="contato.php"> <img class="language-icon" src="images/brazil-icon.png"/> </a>
			<a class="linkporcima" href="contato_eng.php"> <img class="language-icon" src="images/eua-icon.png"/> </a>
		</div>
		<div class="navbar navbar default supercima">
			<div class="container primeiro">
				<div class=" cabecalho">
					<a href="index_eng.php"> 
					<img class="logo" src="images/logo.png" width=20% /></a>
				</div>
				<div class="primeiro2">
					<ul class="nav navbar-tabs nav-justified" role="tablist">
						<li class="botao b1">
							<a class="MenuButtons" href="index_eng.php" role="button">
								Home
							</a>
						</li>
						<li class="botao b2">
							<a class="MenuButtons" href="fotos_eng.php">
								Photos
							</a>
						</li>
						<li class="botao b3">
							<a class="MenuButtons" href="localizacao_eng.php">
								Location
							</a>
						</li>
						<li class="botao b4">
							<a class="MenuButtons" href="contato_eng.php">
								Contact Us
							</a>
						</li>
						<li class="botao b5">
							<a class="MenuButtons" href="pacotes_eng.php">
								Packages
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="container segundo">
			<div class="divcontainer" >
				<h2 style="width: 100%; text-align: center"><b> Contact Tibau Paradise Staff </b></h2>
				<br>
				<br>
				<label style="width: 100%; text-align: center"> To contact us, please fill the fields bellow. Soon we will respond your message. </label>
			</div>
				<br>
				<br>
				<br>
			<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"])?>" method="POST">
				<br> 
				
				<div class="input-group" style="width: 80%; margin: 0 auto">
					<label> <label style="color: red"> * </label> Message: </label> 
					<br>
					<textarea id="id_message" rows="10" name="message" class="form-control" cols="40"></textarea>
					<br>
				</div>
				<br>
				
				<div class="input-group" style="width: 80%; margin: 0 auto">
					<label> <label style="color: red"> * </label> Your e-mail:  </label> 
					<br>
					<input id="id_sender" type="email" class="form-control" size="50" name="from"></input>
				</div>
				<br>
				
				<div class="input-group" style="width: 80%; margin: 0 auto">
					<label> <label style="color: red"> * </label> Your phone number:  </label> 
					<br>
					<input id="id_tel" type="text" class="form-control" size="15" name="tel"></input>
				</div>
				<br>
				
				<div class="input-group" style="width: 80%; margin: 0 auto">
					<label>
					<label style="color: red; font-size: 11px"> * Required fields. </label> <br> <br>
					<input id="id_cc_myself" type="checkbox" name="cc_myself" style="margin-right: 10px"> Send a copy to yourself? </input> </label>
				</div>
				
				<br>
				
				<div class="input-group" style="width: 80%; margin: 0 auto">
					<input type="submit" value="Submit" style="margin: 0 auto"></input>
				</div>
			</form>
		</div>
		
		<div class="site-design">
			<label class="site-design-text"> Site criado por: Gabriel Oliveira. <br> oliveira.grdo@gmail.com </label>
		</div>
	</body>
</html>

<!-- <html>
	<head>
		<title>email</title>
	</head>
	<body>
		<form method="POST" action="/your-name/"> {% csrf_token
			{{ form.as_p }}
			<input type="submit" value="Enviar">
		</form>
	</body>
</html> -->		