<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		
		<title>Tibau Paradise</title>
		<!-- Include meta tag to ensure proper rendering and touch zooming -->
		<meta name="viewport" content="width=device-width, initial-scale=1">
		
		
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="css/bootstrap.css">
		<link rel="stylesheet" href="cabecalho.css">
		<link rel="stylesheet" href="pacotes.css">
		
	</head>

	<body>
		<div class="language">
			<a class="linkporcima" href="pacotes.php"> <img class="language-icon" src="images/brazil-icon.png"/> </a>
			<a class="linkporcima" href="pacotes_eng.php"> <img class="language-icon" src="images/eua-icon.png"/> </a>
		</div>
		<div class="navbar navbar default supercima">
			<div class="container primeiro">
				<div class=" cabecalho">
					<a href="index_eng.php"> 
					<img class="logo" src="images/logo.png" width=20% /></a>
				</div>
				<div class="primeiro2">
					<ul class="nav navbar-tabs nav-justified" role="tablist">
						<li class="botao b1">
							<a class="MenuButtons" href="index_eng.php" role="button">
								Home
							</a>
						</li>
						<li class="botao b2">
							<a class="MenuButtons" href="fotos_eng.php">
								Photos
							</a>
						</li>
						<li class="botao b3">
							<a class="MenuButtons" href="localizacao_eng.php">
								Location
							</a>
						</li>
						<li class="botao b4">
							<a class="MenuButtons" href="contato_eng.php">
								Contact Us
							</a>
						</li>
						<li class="botao b5">
							<a class="MenuButtons" href="pacotes_eng.php">
								Packages
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="container segundo">
		
			<div class="divcontainer">
			
				<label class="text"> The Tibau Paradise Staff, thinking in the well-being of your clients, offers many promotional packages for holidays. Everything to make your stay even better. <br> <br>
				Check out the options available on this page: </label>
				</label>
			
			</div>
			
			<div class="image">
					<img class="img left" src="images/fotos/pacotes-03.jpg">
					<label class="text2"> <li> Soon, more informations. </label>
			</div>
		</div>
		<div class="site-design">
			<label class="site-design-text"> Site criado por: Gabriel Oliveira. <br> oliveira.grdo@gmail.com </label>
		</div>
	</body>
</html>