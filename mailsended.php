<!DOCTYPE html>
<html>
	<head>
		<title>Pousada Ekoo's Beach</title>
		<meta charset="utf-8" />
		
		<link rel="stylesheet" href="css/mailsended.css">
		<link rel="stylesheet" href="css/background.css">
		<link rel="stylesheet" href="css/container.css">
	</head>

	<body>
		
		<?php include 'language.php' ?>

		<?php include 'menu.php' ?>

		<div class="container segundo">
		
			<div class="divcontainer">
				<h2 style="width: 100%; text-align: center"><b> Mensagem enviada com sucesso. </b></h2>
				<br>
				<br>
				<label style="width: 100%; text-align: center"> Em breve responderemos à sua mensagem. </label>
			</div>
				<br>
				<br>
				<br>
		</div>
		
	</body>
	
	<?php include 'site-design.php' ?>
</html>	