<!DOCTYPE html>
<html>
	<head>
		<title>Pousada Ekoo's Beach</title>
		<meta charset="utf-8" />
		
		<link rel="stylesheet" href="css/fotos.css">
		<link rel="stylesheet" href="css/background.css">
		<link rel="stylesheet" href="css/container.css">

		<link rel="stylesheet" href="css/fotos.css" >
		<link rel="stylesheet" href="css/magnific-popup.css" > 
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		<script src="jquery.magnific-popup.js" ></script>
	</head>

	<body>
		
		<?php include 'language.php' ?>

		<?php include 'menu_eng.php' ?>

		<div class="container">
			<table>
				<tr>
					<td>
						<a href="images/fotos/fotos-foto (2).jpg" title="Foto 02" data-source="images/fotos/fotos-foto (2).jpg" ><img class="image" src="images/fotos/fotos-foto-mini (2).jpg"></a>
					</td>
					<td>
						<a href="images/fotos/fotos-foto (3).jpg" title="Foto 03" data-source="images/fotos/fotos-foto (3).jpg" ><img class="image" src="images/fotos/fotos-foto-mini (3).jpg"></a>
					</td>
					<td>
						<a href="images/fotos/fotos-foto (4).jpg" title="Foto 04" data-source="images/fotos/fotos-foto (4).jpg" ><img class="image" src="images/fotos/fotos-foto-mini (4).jpg"></a>
					</td>
				</tr>
				<tr>
					<td>
						<a href="images/fotos/fotos-foto (5).jpg" title="Foto 05" data-source="images/fotos/fotos-foto (5).jpg" ><img class="image" src="images/fotos/fotos-foto-mini (5).jpg"></a>
					</td>
					<td>
						<a href="images/fotos/fotos-foto (6).jpg" title="Foto 06" data-source="images/fotos/fotos-foto (6).jpg" ><img class="image" src="images/fotos/fotos-foto-mini (6).jpg" /></a>
					</td>
					<td>
						<a href="images/fotos/fotos-foto (7).jpg" title="Foto 07" data-source="images/fotos/fotos-foto (7).jpg" ><img class="image" src="images/fotos/fotos-foto-mini (7).jpg" /></a>
					</td>
				</tr>
				<tr>
					<td>
						<a href="images/fotos/fotos-foto (8).jpg" title="Foto 08" data-source="images/fotos/fotos-foto (8).jpg" ><img class="image" src="images/fotos/fotos-foto-mini (8).jpg" /></a>
					</td>
					<td>
						<a href="images/fotos/fotos-foto (9).jpg" title="Foto 09" data-source="images/fotos/fotos-foto (9).jpg" ><img class="image" src="images/fotos/fotos-foto-mini (9).jpg" /></a>
					</td>
					<td>
						<a href="images/fotos/fotos-foto (10).jpg" title="Foto 10" data-source="images/fotos/fotos-foto (10).jpg" ><img class="image" src="images/fotos/fotos-foto-mini (10).jpg" /></a>
					</td>
				</tr>
				<tr>
					<td>
						<a href="images/fotos/fotos-foto (11).jpg" title="Foto 11" data-source="images/fotos/fotos-foto (11).jpg" ><img class="image" src="images/fotos/fotos-foto-mini (11).jpg" /></a>
					</td>
					<td>
						<a href="images/fotos/fotos-foto (13).jpg" title="Foto 13" data-source="images/fotos/fotos-foto (13).jpg" ><img class="image" src="images/fotos/fotos-foto-mini (13).jpg" /></a>
					</td>
					<td>
						<a href="images/fotos/fotos-foto (14).jpg" title="Foto 14" data-source="images/fotos/fotos-foto (14).jpg" ><img class="image" src="images/fotos/fotos-foto-mini (14).jpg" /></a>
					</td>
				</tr>
			</table>
		</div>
		
		<?php include 'site-design.php' ?>
	</body>
</html>