<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="css/cabecalho.css">
	</head>
	<body>
		<div class="first-div">

			<div class="cabecalho">
				<a href="index.php"> 
				<img class="logo" src="images/logo.png"/></a>
			</div>

			<table class="menu">
				<tr>
					<td class="botao b1">
						<a class="MenuButtons" href="index_eng.php" role="button">
							Home
						</a>
					</td>
					<td class="botao b2">
						<a class="MenuButtons" href="fotos_eng.php">
							Photos
						</a>
					</td>
					<td class="botao b3">
						<a class="MenuButtons" href="localizacao_eng.php">
							Location
						</a>
					</td>
					<td class="botao b4">
						<a class="MenuButtons" href="contato_eng.php">
							Contact Us
						</a>
					</td>
					<td class="botao b5">
						<a class="MenuButtons" href="pacotes_eng.php">
							Packages
						</a>
					</td>
				</tr>
			</table>

		</div>
	</body>
</html>