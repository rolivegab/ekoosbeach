<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		
		<title>Tibau Paradise</title>
		<!-- Include meta tag to ensure proper rendering and touch zooming -->
		<meta name="viewport" content="width=device-width, initial-scale=1">
		
		
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
		<link rel="stylesheet" href="cabecalho.css">
		<link rel="stylesheet" href="mailSended.css">
		
	</head>

	<body>
		<div class="language">
			<a class="linkporcima" href="mailSended.php"> <img class="language-icon" src="images/brazil-icon.png"/> </a>
			<a class="linkporcima" href="mailSended_eng.php"> <img class="language-icon" src="images/eua-icon.png"/> </a>
		</div>
		<div class="navbar navbar default supercima">
			<div class="container primeiro">
				<div class=" cabecalho">
					<a href="index_eng.php"> 
					<img class="logo" src="images/logo.png" width=20% /></a>
				</div>
				<div class="primeiro2">
					<ul class="nav navbar-tabs nav-justified" role="tablist">
						<li class="botao b1">
							<a class="MenuButtons" href="index_eng.php" role="button">
								Home
							</a>
						</li>
						<li class="botao b2">
							<a class="MenuButtons" href="fotos_eng.php">
								Photos
							</a>
						</li>
						<li class="botao b3">
							<a class="MenuButtons" href="localizacao_eng.php">
								Location
							</a>
						</li>
						<li class="botao b4">
							<a class="MenuButtons" href="contato_eng.php">
								Contact Us
							</a>
						</li>
						<li class="botao b5">
							<a class="MenuButtons" href="pacotes_eng.php">
								Packages
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="container segundo">
		
			<div class="divcontainer">
				<h2 style="width: 100%; text-align: center"><b> Message sent successfully. </b></h2>
				<br>
				<br>
				<label style="width: 100%; text-align: center"> Soon we will reply your message. </label>
			</div>
				<br>
				<br>
				<br>
		</div>
		
	</body>
	
	<div class="site-design">
		<label class="site-design-text"> Site criado por: Gabriel Oliveira. <br> oliveira.grdo@gmail.com </label>
	</div>
</html>	

<!-- <html>
	<head>
		<title>email</title>
	</head>
	<body>
		<form method="POST" action='/your-name/'> {% csrf_token
			{{ form.as_p }}
			<input type="submit" value="Enviar">
		</form>
	</body>
</html> -->