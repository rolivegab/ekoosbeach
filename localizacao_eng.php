<!DOCTYPE html>
<html>
	<head>
		<title>Pousada Ekoo's Beach</title>
		<meta charset="utf-8">
		
		<link rel="stylesheet" href="css/localizacao.css">
		<link rel="stylesheet" href="css/background.css">
		<link rel="stylesheet" href="css/container.css">
		<script>
			function initialize() {
				var myLatLng = new google.maps.LatLng(-6.187516, -35.090851);
				var mapOptions = {
					zoom: 18,
					center: myLatLng
				}

				var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

				var marker = new google.maps.Marker({
					position: myLatLng,
					map: map,
					title: 'Pousada Ekoo\'s Beach!'
				});
			}
		</script>
	</head>

	<body>
		<?php include 'language.php' ?>
		
		<?php include 'menu.php' ?>

		<div class="container segundo">
			
			<div class="divcontainer">
			
				<label class="text"> Find the Tibau Paradise easily with Google Maps. </label>
				</label>
			
			</div>
		
			<div class="mapa" id="map-canvas"></div>
			<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDWBeLjmBOODCPVlEK5b-JnItcD_6DqmA0&callback=initialize" type="text/javascript"></script>
		</div>
		
		<?php include 'site-design.php' ?>
		
	</body>
</html>