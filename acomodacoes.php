<!DOCTYPE html>
<html>
	<head>
		<title>Pousada Ekoo's Beach</title>
		<meta charset="utf-8" />
		
		<link rel="stylesheet" href="css/acomodacoes.css">
		<link rel="stylesheet" href="css/background.css">
		<link rel="stylesheet" href="css/container.css">
		<link rel="stylesheet" href="css/cabecalho.css">

		<script src="css/jquery.magnific-popup.js" ></script>
	</head>

	<body>
		
		<?php include 'language.php' ?>

		<?php include 'menu.php' ?>

		<div class="container">
			<table>
				<tr>
					<td>
						<a href="files/ekoos22.jpg" title="Foto 01" data-source="files/ekoos22.jpg" ><img class="image" src="files/ekoos22-ed.jpg"></a>
					</td>
					<td>
						<a href="files/ekoos24.jpg" title="Foto 01" data-source="files/ekoos24.jpg" ><img class="image" src="files/ekoos24-ed.jpg"></a>
					</td>
					<td>
						<a href="files/ekoos25.jpg" title="Foto 01" data-source="files/ekoos25.jpg" ><img class="image" src="files/ekoos25-ed.jpg"></a>
					</td>
				</tr>
				<tr>
					<td>
						<a href="files/ekoos26.jpg" title="Foto 01" data-source="files/ekoos26.jpg" ><img class="image" src="files/ekoos26-ed.jpg"></a>
					</td>
					<td>
						<a href="files/ekoos28.jpg" title="Foto 01" data-source="files/ekoos28.jpg" ><img class="image" src="files/ekoos28-ed.jpg"></a>
					</td>
					<td>
						<a href="files/ekoos30.jpg" title="Foto 01" data-source="files/ekoos30.jpg" ><img class="image" src="files/ekoos30-ed.jpg"></a>
					</td>
				</tr>
				<tr>
					<td>
						<a href="files/ekoos31.jpg" title="Foto 01" data-source="files/ekoos31.jpg" ><img class="image" src="files/ekoos31-ed.jpg"></a>
					</td>
					<td>
						<a href="files/ekoos32.jpg" title="Foto 01" data-source="files/ekoos32.jpg" ><img class="image" src="files/ekoos32-ed.jpg"></a>
					</td>
					<td>
						<a href="files/ekoos34.jpg" title="Foto 01" data-source="files/ekoos34.jpg" ><img class="image" src="files/ekoos34-ed.jpg"></a>
					</td>
				</tr>
				<tr>
					<td>
						<a href="files/ekoos35.jpg" title="Foto 01" data-source="files/ekoos35.jpg" ><img class="image" src="files/ekoos35-ed.jpg"></a>
					</td>
				</tr>
			</table>
		</div>
		
		<?php include 'site-design.php' ?>

	</body>
</html>