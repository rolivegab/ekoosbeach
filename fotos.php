<!DOCTYPE html>
<html>
	<head>
		<title>Pousada Ekoo's Beach</title>
		<meta charset="utf-8" />
		
		<link rel="stylesheet" href="css/fotos.css">
		<link rel="stylesheet" href="css/background.css">
		<link rel="stylesheet" href="css/container.css">
		<link rel="stylesheet" href="css/cabecalho.css">

		<script src="css/jquery.magnific-popup.js" ></script>
	</head>

	<body>
		
		<?php include 'language.php' ?>

		<?php include 'menu.php' ?>

		<div class="container">
			<table>
				<tr>
					<td>
						<a href="files/ekoos02.jpg" title="Foto 02" data-source="images/fotos/fotos-foto (2).jpg" ><img class="image" src="files/ekoos02-ed.jpg"></a>
					</td>
					<td>
						<a href="files/ekoos03.jpg" title="Foto 03" data-source="images/fotos/fotos-foto (3).jpg" ><img class="image" src="files/ekoos03-ed.jpg"></a>
					</td>
					<td>
						<a href="files/ekoos05.jpg" title="Foto 04" data-source="images/fotos/fotos-foto (4).jpg" ><img class="image" src="files/ekoos05-ed.jpg"></a>
					</td>
				</tr>
				<tr>
					<td>
						<a href="files/ekoos06.jpg" title="Foto 05" data-source="images/fotos/fotos-foto (5).jpg" ><img class="image" src="files/ekoos06-ed.jpg"></a>
					</td>
					<td>
						<a href="files/ekoos07.jpg" title="Foto 06" data-source="images/fotos/fotos-foto (6).jpg" ><img class="image" src="files/ekoos07-ed.jpg" /></a>
					</td>
					<td>
						<a href="files/ekoos09.jpg" title="Foto 07" data-source="images/fotos/fotos-foto (7).jpg" ><img class="image" src="files/ekoos09-ed.jpg" /></a>
					</td>
				</tr>
				<tr>
					<td>
						<a href="files/ekoos10.jpg" title="Foto 08" data-source="images/fotos/fotos-foto (8).jpg" ><img class="image" src="files/ekoos10-ed.jpg" /></a>
					</td>
					<td>
						<a href="files/ekoos11.jpg" title="Foto 09" data-source="images/fotos/fotos-foto (9).jpg" ><img class="image" src="files/ekoos11-ed.jpg" /></a>
					</td>
					<td>
						<a href="files/ekoos12.jpg" title="Foto 10" data-source="images/fotos/fotos-foto (10).jpg" ><img class="image" src="files/ekoos12-ed.jpg" /></a>
					</td>
				</tr>
				<tr>
					<td>
						<a href="files/ekoos13.jpg" title="Foto 11" data-source="images/fotos/fotos-foto (11).jpg" ><img class="image" src="files/ekoos13-ed.jpg" /></a>
					</td>
					<td>
						<a href="files/ekoos14.jpg" title="Foto 13" data-source="images/fotos/fotos-foto (13).jpg" ><img class="image" src="files/ekoos14-ed.jpg" /></a>
					</td>
					<td>
						<a href="files/ekoos15.jpg" title="Foto 14" data-source="images/fotos/fotos-foto (14).jpg" ><img class="image" src="files/ekoos15-ed.jpg" /></a>
					</td>
				</tr>
				<tr>
					<td>
						<a href="files/ekoos16.jpg" title="Foto 11" data-source="images/fotos/fotos-foto (11).jpg" ><img class="image" src="files/ekoos16-ed.jpg" /></a>
					</td>
					<td>
						<a href="files/ekoos17.jpg" title="Foto 13" data-source="images/fotos/fotos-foto (13).jpg" ><img class="image" src="files/ekoos17-ed.jpg" /></a>
					</td>
					<td>
						<a href="files/ekoos18.jpg" title="Foto 14" data-source="images/fotos/fotos-foto (14).jpg" ><img class="image" src="files/ekoos18-ed.jpg" /></a>
					</td>
				</tr>
				<tr>
					<td>
						<a href="files/ekoos19.jpg" title="Foto 11" data-source="images/fotos/fotos-foto (11).jpg" ><img class="image" src="files/ekoos19-ed.jpg" /></a>
					</td>
					<td>
						<a href="files/ekoos20.jpg" title="Foto 13" data-source="images/fotos/fotos-foto (13).jpg" ><img class="image" src="files/ekoos20-ed.jpg" /></a>
					</td>
					<td>
						<a href="files/ekoos21.jpg" title="Foto 14" data-source="images/fotos/fotos-foto (14).jpg" ><img class="image" src="files/ekoos21-ed.jpg" /></a>
					</td>
				</tr>
				<tr>
					<td>
						<a href="files/ekoos22.jpg" title="Foto 11" data-source="images/fotos/fotos-foto (11).jpg" ><img class="image" src="files/ekoos22-ed.jpg" /></a>
					</td>
					<td>
						<a href="files/ekoos23.jpg" title="Foto 13" data-source="images/fotos/fotos-foto (13).jpg" ><img class="image" src="files/ekoos23-ed.jpg" /></a>
					</td>
					<td>
						<a href="files/ekoos36.jpg" title="Foto 14" data-source="images/fotos/fotos-foto (14).jpg" ><img class="image" src="files/ekoos36-ed.jpg" /></a>
					</td>
				</tr>
				<tr>
					<td>
						<a href="files/ekoos37.jpg" title="Foto 11" data-source="images/fotos/fotos-foto (11).jpg" ><img class="image" src="files/ekoos37-ed.jpg" /></a>
					</td>
					<td>
						<a href="files/ekoos38.jpg" title="Foto 13" data-source="images/fotos/fotos-foto (13).jpg" ><img class="image" src="files/ekoos38-ed.jpg" /></a>
					</td>
					<td>
						<a href="files/ekoos39.jpg" title="Foto 14" data-source="images/fotos/fotos-foto (14).jpg" ><img class="image" src="files/ekoos39-ed.jpg" /></a>
					</td>
				</tr>
				<tr>
					<td>
						<a href="files/ekoos40.jpg" title="Foto 11" data-source="images/fotos/fotos-foto (11).jpg" ><img class="image" src="files/ekoos40-ed.jpg" /></a>
					</td>
					<td>
						<a href="files/ekoos41.jpg" title="Foto 13" data-source="images/fotos/fotos-foto (13).jpg" ><img class="image" src="files/ekoos41-ed.jpg" /></a>
					</td>
					<td>
						<a href="files/ekoos42.jpg" title="Foto 14" data-source="images/fotos/fotos-foto (14).jpg" ><img class="image" src="files/ekoos42-ed.jpg" /></a>
					</td>
				</tr>
				<tr>
					<td>
						<a href="files/ekoos43.jpg" title="Foto 11" data-source="images/fotos/fotos-foto (11).jpg" ><img class="image" src="files/ekoos43-ed.jpg" /></a>
					</td>
					<td>
						<a href="files/ekoos44.jpg" title="Foto 13" data-source="images/fotos/fotos-foto (13).jpg" ><img class="image" src="files/ekoos44-ed.jpg" /></a>
					</td>
					<td>
						<a href="files/ekoos47.jpg" title="Foto 13" data-source="images/fotos/fotos-foto (13).jpg" ><img class="image" src="files/ekoos47-ed.jpg" /></a>
					</td>
				</tr>
				<tr>
					<td>
						<a href="files/ekoos48.jpg" title="Foto 14" data-source="images/fotos/fotos-foto (14).jpg" ><img class="image" src="files/ekoos48-ed.jpg" /></a>
					</td>
					<td>
						<a href="files/ekoos49.jpg" title="Foto 11" data-source="images/fotos/fotos-foto (11).jpg" ><img class="image" src="files/ekoos49-ed.jpg" /></a>
					</td>
				</tr>
			</table>
		</div>
		
		<?php include 'site-design.php' ?>

	</body>
</html>