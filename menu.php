<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="css/cabecalho.css">
	</head>
	<body>
		<div class="first-div">

			<div class="cabecalho">
				<a href="index.php"><img class="logo" src="images/logo.png"/></a>
			</div>

			<div class="telephone">
				Telefones:
				<br>
				55 84 3246 4080 / 99866 4057
			</div>
			<table class="menu">
				<tr>
					<td class="botao b1">
						<a class="MenuButtons" href="index.php" role="button">
							Início
						</a>
					</td>
					<td class="botao b2">
						<a class="MenuButtons" href="fotos.php">
							Fotos
						</a>
					</td>
					<td class="botao b3">
						<a class="MenuButtons" href="acomodacoes.php">
							Acomodações
						</a>
					</td>
					<td class="botao b4">
						<a class="MenuButtons" href="localizacao.php">
							Localização
						</a>
					</td>
					<td class="botao b5">
						<a class="MenuButtons" href="contato.php">
							Contato
						</a>
					</td>
					<td class="botao b6">
						<a class="MenuButtons" href="pacotes.php">
							Pacotes
						</a>
					</td>
				</tr>
			</table>

		</div>
	</body>
</html>