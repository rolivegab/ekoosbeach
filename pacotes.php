<!DOCTYPE html>
<html>
	<head>
		<title>Pousada Ekoo's Beach</title>
		<meta charset="utf-8" />
		
		<link rel="stylesheet" href="css/background.css">
		<link rel="stylesheet" href="css/container.css">
		<link rel="stylesheet" href="css/cabecalho.css">

		<link rel="stylesheet" href="css/pacotes.css">
		
	</head>

	<body>
		<?php include 'language.php' ?>

		<?php include 'menu.php' ?>

		<div class="container segundo">
		
			<div class="divcontainer">
			
				<label class="text"> A Equipe da Ekoo's Beach, pensando no bem estar dos seus clientes, dispões de vários pacotes promocionais para as datas comemorativas mais importantes. Tudo para deixar sua estadia ainda melhor. <br> <br>
				Confira nesta página as opções disponíveis: </label>
				</label>
			
			</div>
			
			<div class="image">
					<img class="img left" src="images/fotos/pacotes-03.jpg">
					<label class="text2"> <li> Em breve mais informações.					</label>
			</div>
		</div>
		
		<?php include 'site-design.php' ?>

	</body>
</html>