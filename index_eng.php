<!DOCTYPE html>
<html>
	<head>
		<title>Pousada Ekoo's Beach</title>
		<meta charset="utf-8" />
		
		<link rel="stylesheet" href="css/index.css">
		<link rel="stylesheet" href="css/background.css">
		<link rel="stylesheet" href="css/container.css">
	</head>

	<body>
		<?php include 'language.php' ?>

		<?php include 'menu_eng.php' ?>

		<div class="container">
			<div class="image">
				<img class="img left" src="images/fotos/index-01.jpg">
				<label class="text"> The Tibau Paradise Hotel, located in the center of Tibau do Sul, offer all the comfort and structure that your pleasure needs. Only 5 minutes from Praia da Pipa, and 300 meters from Praia do Giz and the beautiful Lagoa de Guaraíras, where you can enjoy the most beautiful sunset of RN. </label>
			</div>
			<div class="image">
				<img class="img right" src="images/fotos/index-02.jpg">
				<label class="text"> Our structure is made of cottages, all with bathroom, TV, air conditioning, minibar, and internet. The hotel has as well a recreation area with pool and barbecue, that you can use at will. Furthermore, we offer an pleasant ambient, with many plants, flowers and a garden, perfect for a break. </label>
			</div>
			<div class="image">
				<img class="img left" src="images/fotos/index-03.jpg">
				<label class="text"> The region has many options for entertainment, from buggy rides through the dunes up to boat and Kayak rides through the Lagoa de Guaraíras. Besides, there are plenty of regional cuisine to taste. To complete, the bars and nightclubs from Pipa offer entertainment for all night.	 </label>
			</div>

			<br>
			<br>
			
			<?php include 'social.php' ?>

		</div>

		<?php include 'site-design.php' ?>
	</body>
</html>